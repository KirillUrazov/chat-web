import React, { Component } from 'react';
// import { connect } from 'react-redux';
// import { sendMessage } from './actions/actions';
// import { sendMessage } from './redux';
import Chat from './containers/Dialog';
import DialogsList from './containers/DialogsList';

// App.js
export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {dialogID: null};
        this.changeChat = this.changeChat.bind(this);
    }

    changeChat(newDialogID) {
        this.setState({
            dialogID: newDialogID
        });
    }

    render() {
        return (
            <div>
                {this.state.dialogID ?
                    <Chat dialog={this.props.chatReducer.dialogs[this.state.dialogID]}
                          sendMessage={this.props.sendMessage}
                          back={this.changeChat}
                    />
                    : <DialogsList
                        dialogs={this.props.chatReducer.dialogs}
                        changeChat={this.changeChat}
                        owner={this.props.chatReducer.owner}
                    />
                }
            </div>
        );
    }

}

// // AppContainer.js
// const mapStateToProps = (state, ownProps) => ({
//     chatReducer: state.chatReducer,
//     dialogID: state.dialogID
// });
//
// const mapDispatchToProps = {
//     sendMessage
// };
//
// const AppContainer = connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(App);
//
// export default AppContainer;
