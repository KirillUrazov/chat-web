export function sendMessage(text, dialogID) {
    return {
        type: 'SEND_MESSAGE',
        text,
        dialogID
    };
}
