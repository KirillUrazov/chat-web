import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Footer extends Component {
    constructor(props) {
        super(props);
        this.handleButtonClick = this.handleButtonClick.bind(this);
        this.handleInputKeyDown = this.handleInputKeyDown.bind(this);
        this.handleInputKeyUp = this.handleInputKeyUp.bind(this);
        this.fixHeight = this.fixHeight.bind(this);
    }

    handleButtonClick() {
        let inputMessage = document.querySelector('[name="message"]');
        let text = inputMessage ? inputMessage.value : '';
        if (text !== '') {
            this.props.sendMessage(text, this.props.dialogID);
            inputMessage.value = '';
            this.fixHeight(inputMessage);
        }
    }

    handleInputKeyDown(e) {
        switch (e.keyCode) {
            case 13:
                //Если нажали ctrl + enter
                if (e.ctrlKey) {
                    this.handleButtonClick(e);
                }
                break;
            default:
        }
    }

    flexibleTextarea() {
        // let textarea = document.querySelector('[name="message"]'),
        //     style = textarea.style,
        //     main = document.querySelector('.chat-container main'),
        //     footer = document.querySelector('.chat-container footer');
        // style.overflow = 'hidden';
        // style.wordWrap = 'break-word';
        // console.log(this);
        // textarea.onkeyup = function () {
        //     if (/*@cc_on!@*/1) {
        //         style.height = 'auto';
        //     }
        //     console.log(this);
        //     style.height = this.scrollHeight + 'px';
        //     main.style.height = 'calc(100% - 50px - ' + footer.offsetHeight + 'px)';
        //     document.querySelector('main').scrollTop = 999999;
        // };
        //
        // textarea.onscroll = function () {
        //     console.log(this);
        //     this.scrollTop = 0;
        // };
    }

    handleInputKeyUp(e) {
        this.fixHeight(e.target);
    }

    fixHeight(textarea) {
        const style = textarea.style,
            main = document.querySelector('.chat-container main'),
            footer = document.querySelector('.chat-container footer');
        if (/*@cc_on!@*/1) {
            style.height = 'auto';
        }
        style.height = textarea.scrollHeight + 'px';
        main.style.height = 'calc(100% - 50px - ' + footer.offsetHeight + 'px)';
        document.querySelector('main').scrollTop = 999999;
    }

    componentDidMount() {
        document.querySelector('main').scrollTop = 999999;
    }

    componentDidUpdate() {
        document.querySelector('main').scrollTop = 999999;
    }


    render() {
        return (
            <footer>
                    <textarea
                        name="message"
                        id="message"
                        rows="1"
                        onKeyUp={this.handleInputKeyUp}
                        onKeyDown={this.handleInputKeyDown}
                        placeholder="Message"
                        autoFocus={true}
                    />
                <button onClick={this.handleButtonClick}><span className="sr-only">Send Message</span></button>
            </footer>
        );
    }
}

Footer.propTypes = {
    sendMessage: PropTypes.func.isRequired,
    dialogID: PropTypes.number.isRequired
};