import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Menu from './Menu';

let menu;

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.back = this.back.bind(this);
        this.openMenu = this.openMenu.bind(this);
    }

    back(e) {
        e.preventDefault();
        this.props.back();
    }

    openMenu(e) {
        e.preventDefault();
        menu.classList.toggle('open');
    }

    componentDidMount() {
        menu = document.querySelector('.dialog-menu');
    }

    render() {
        return (
            <header>
                <a href="" className="back-btn" onClick={this.back}>
                    <img src="/assets/images/back1600.png" alt="" />
                    <span className="sr-only">Back</span>
                </a>
                <div className="avatar">
                    <img src={this.props.dialog.user.avatar} alt="avatar" />
                </div>
                <div className="username">
                    <span>{this.props.dialog.user.name}</span>
                    <br /><span className="last-active">был в сети вчера 17:17</span>
                </div>
                <a href="" className="kebab-menu" onFocus={this.openMenu} onBlur={this.openMenu}>
                    <img src="/assets/images/kebab.png" alt="" />
                    <span className="sr-only">Menu</span>
                </a>
                <Menu />
            </header>
        );
    }
}

Header.propTypes = {
    dialog: PropTypes.object.isRequired,
    back: PropTypes.func.isRequired
};
