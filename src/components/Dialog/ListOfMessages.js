import React, { Component } from 'react';
import Message from './Message';

export default class ListOfMessages extends Component {
    render() {
        let msgs = this.props.dialog.messages,
            messagesArray = [];
        for (let key in msgs) {
            if(!msgs.hasOwnProperty(key)) continue;
            messagesArray.push(
                <Message
                    key={msgs[key].id}
                    text={msgs[key].text}
                    sent={msgs[key].sent}
                    time={msgs[key].time}
                />
            );
        }
        return <main>{messagesArray}</main>;
    }
}

ListOfMessages.propTypes = {
    dialog: React.PropTypes.object.isRequired
};