import React, {Component} from 'react';

export default class Menu extends Component {
    render() {
        return (
            <div className="dialog-menu">
                <ul>
                    <li>Call</li>
                    <li>Search</li>
                    <li>Clear history</li>
                    <li>Delete chat</li>
                    <li>Mute notifications</li>
                </ul>
            </div>
        );
    }
}