import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Message extends Component {
    render() {
        return (
            <div className="message-container">
                <div className={this.props.sent ? 'message-sent' : 'message'}>
                    <p>{this.props.text}</p>
                    <p className="date-time">{this.props.time}</p>
                </div>
            </div>
        );
    }
}

Message.propType = {
    text: PropTypes.string.isRequired,
    sent: PropTypes.bool.isRequired,
    time: PropTypes.string.isRequired
};