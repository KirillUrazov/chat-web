import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.handleMenuClick = this.handleMenuClick.bind(this);
    }

    handleMenuClick(e) {
        e.preventDefault();
        this.props.openMenu(e);
    }

    search(e) {
        e.preventDefault();
        /*
        TODO: Дописать функцию показа/скрытия страницы поиска чатов и написать сам поиск
         */
    }

    render() {
        return (
            <header>
                <a href="" className="menu-btn" onClick={this.handleMenuClick}>
                    <img src="/assets/images/menu.png" alt="" />
                    <span className="sr-only">Menu</span>
                </a>
                <div className="title"><span>Freematiq Chat</span></div>
                <a href="" className="search" onClick={this.search}>
                    <img src="/assets/images/search.png" alt="" />
                    <span className="sr-only">Search</span>
                </a>
            </header>
        );
    }
}

Header.propTypes = {
    openMenu: PropTypes.func.isRequired
};