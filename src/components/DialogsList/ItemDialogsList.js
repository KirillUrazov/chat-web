import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ItemDialogsList extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.changeChat(this.props.id);
    }

    render() {
        return (
            <div className="item_from_list_of_chats-container" onClick={this.handleClick}>
                <div className="avatar">
                    <img src={this.props.avatar} alt={this.props.name} />
                </div>
                <div className="content">
                    <div className="username">{this.props.name}</div>
                    <div className="message">{this.props.message}</div>
                </div>
                <div className="date-time">{String(this.props.dateTime)}</div>
            </div>
        );
    }
}

ItemDialogsList.propTypes = {
    id: PropTypes.number.isRequired,
    avatar: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    dateTime: PropTypes.string.isRequired,
    changeChat: PropTypes.func.isRequired
};