import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MenuHeader from './MenuHeader';
import MenuNav from './MenuNav';

export default class Menu extends Component {
    render() {
        return (
            <div className="menu-container">
                <MenuHeader user={this.props.user} />
                <MenuNav />
            </div>
        );
    }
}

Menu.propTypes = {
    user: PropTypes.object.isRequired
};
