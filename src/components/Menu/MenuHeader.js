import React from 'react';
import PropTypes from 'prop-types';

export default function MenuHeader(props) {

    return (
        <header>
            <div className="avatar">
                <img src={props.user.avatar} alt={props.user.name.charAt(0).toUpperCase()} />
            </div>
            <div className="username">
                <span className="name">{props.user.name}</span>
                <br /><span style={{color: 'rgb(240, 240, 255'}}>+7-913-213-99-16</span>
            </div>
            <div className="chat-with-yourself"/>
        </header>
    );
}

MenuHeader.propTypes = {
    user: PropTypes.object.isRequired
};
