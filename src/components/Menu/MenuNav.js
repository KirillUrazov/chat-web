import React from 'react';

export default function MenuNav() {

    return (
        <nav>
            <ul>
                <li><span className="icon icon-supervisor_account"/>New Group</li>
                <li><span className="icon icon-lock"/>New Secret Chat</li>
                <li><span className="icon icon-video_call"/>New Channel</li>
            </ul>
            <ul>
                <li><span className="icon icon-account_box"/>Contacts</li>
                <li><span className="icon icon-phone"/>Calls</li>
                <li><span className="icon icon-person_add"/>Invite Friends</li>
                <li><span className="icon icon-settings"/>Settings</li>
                <li><span className="icon icon-help"/>Help</li>
            </ul>
        </nav>

    );
}