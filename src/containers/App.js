import { connect } from 'react-redux';
import {sendMessage} from '../actions/actions';
import App from '../App';


const mapStateToProps = (state, ownProps) => ({
    chatReducer: state.chatReducer,
    dialogID: state.dialogID
});

const mapDispatchToProps = {
    sendMessage
};

const AppContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(App);

export default AppContainer;
