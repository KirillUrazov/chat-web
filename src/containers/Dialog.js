import React, { Component } from 'react';
import Header from '../components/Dialog/Header';
import ListOfMessages from '../components/Dialog/ListOfMessages';
import Footer from '../components/Dialog/Footer';
import PropTypes from 'prop-types';

export default class Chat extends Component {
    render() {
        return (
            <div className="chat-container">
                <Header dialog={this.props.dialog} back={this.props.back} />
                <ListOfMessages dialog={this.props.dialog} />
                <Footer sendMessage={this.props.sendMessage} dialogID={this.props.dialog.id} />
            </div>
        );
    }
}

Chat.propType = {
    dialog: PropTypes.object.isRequired,
    back: PropTypes.func.isRequired,
    sendMessage: PropTypes.func.isRequired
};