import React, { Component } from 'react';
import ItemDialogsList from '../components/DialogsList/ItemDialogsList';
import PropTypes from 'prop-types';
import Menu from '../components/Menu/Menu';
import Header from '../components/DialogsList/Header';

const overlay = document.createElement('div');

export default class DialogsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currChat: null
        };
        this.changeChat = this.changeChat.bind(this);
        this.openMenu = this.openMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
    }

    changeChat(newChatID) {
        this.props.changeChat(newChatID);
    }

    openMenu() {
        overlay.classList.add('open');
        document.querySelector('.menu-container').classList.add('open');
    }

    closeMenu() {
        overlay.classList.remove('open');
        document.querySelector('.menu-container').classList.remove('open');
    }

    componentDidMount() {
        document.body.appendChild(overlay);
        overlay.addEventListener('click', this.closeMenu);
        overlay.classList.add('overlay');
    }

    render() {
        let items = [];
        for (let key in this.props.dialogs) {
            if (!this.props.dialogs.hasOwnProperty(key)) continue;
            let currDialog = this.props.dialogs[key],
                lastMsgKey = 0;
            for (let keyMsg in currDialog.messages) {
                lastMsgKey = keyMsg;
            }

            items.push(
                <ItemDialogsList
                    key={currDialog.id}
                    id={currDialog.id}
                    avatar={currDialog.user.avatar}
                    name={currDialog.user.name}
                    message={currDialog.messages[lastMsgKey].sent ? 'Вы: ' : '' + currDialog.messages[lastMsgKey].text}
                    dateTime={currDialog.messages[lastMsgKey].time}
                    changeChat={this.changeChat}
                />
            );
        }
        return (
            <div>
                <Menu user={this.props.owner} />
                <div className="list_of_chats-container">
                    <Header openMenu={this.openMenu} />
                    <div>{items}</div>
                </div>
            </div>
        );
    }
}

DialogsList.propType = {
    dialogs: PropTypes.object.isRequired,
    changeChat: PropTypes.func.isRequired
};