import { combineReducers } from 'redux';

const initialState = {
    owner: {
        name: 'Кирилл',
        avatar: ''
    },
    dialogs: {
        1: {
            id: 1,
            user: {
                name: 'Василий Иванович',
                avatar: '//vk.com/images/stickers/102/128.png',
            },
            messages: {
                1: {
                    id: 1,
                    sent: false,
                    text: 'I\'m first message',
                    time: '6:12'
                },
                2: {
                    id: 2,
                    sent: true,
                    text: 'I\'m second message',
                    time: '6:12'
                },
                3: {
                    id: 3,
                    sent: false,
                    text: 'I\'m third message',
                    time: '6:12'
                },
                4: {
                    id: 4,
                    sent: true,
                    text: 'I\'m second message',
                    time: '6:12'
                },
                5: {
                    id: 5,
                    sent: false,
                    text: 'I\'m third message',
                    time: '6:12'
                },
                6: {
                    id: 6,
                    sent: true,
                    text: 'I\'m second message',
                    time: '6:12'
                },
                7: {
                    id: 7,
                    sent: false,
                    text: 'I\'m third message',
                    time: '6:12'
                },
                8: {
                    id: 8,
                    sent: true,
                    text: 'I\'m second message',
                    time: '6:12'
                },
                9: {
                    id: 9,
                    sent: false,
                    text: 'I\'m third message',
                    time: '6:12'
                }
            },
        },
        2: {
            id: 2,
            user: {
                name: 'Бин',
                avatar: 'https://vk.com/images/stickers/5151/128.png',
            },
            messages: {
                1: {
                    id: 1,
                    sent: false,
                    text: 'Lorem ipsum dolor sit amet',
                    time: '6:12'
                },
                2: {
                    id: 2,
                    sent: false,
                    text: 'consectetur adipiscing elit',
                    time: '6:12'
                },
                3: {
                    id: 3,
                    sent: true,
                    text: 'Proin aliquam sagittis cursus',
                    time: '6:12'
                },
                4: {
                    id: 4,
                    sent: true,
                    text: 'Praesent sit amet tempor lectus. Nulla malesuada magna purus, ut cursus diam dignissim sed. Aliquam sed nibh et felis molestie euismod. Integer dignissim vestibulum enim. Vivamus et cursus enim. Morbi vitae dictum erat.',
                    time: '6:12'
                },
                5: {
                    id: 5,
                    sent: false,
                    text: 'Lorem',
                    time: '6:12'
                }
            },
        },
        3: {
            id: 3,
            user: {
                name: 'Goerg',
                avatar: '//vk.com/images/stickers/3654/128.png',
            },
            messages: {
                1: {
                    id: 1,
                    sent: false,
                    text: 'Morbi a dapibus nunc. Maecenas bibendum vitae diam id laoreet.',
                    time: '6:12'
                },
                2: {
                    id: 2,
                    sent: true,
                    text: 'Nullam sollicitudin neque velit, posuere tempor risus pretium in. Mauris consequat sem sit amet tortor pellentesque elementum.',
                    time: '6:45'
                },
                3: {
                    id: 3,
                    sent: false,
                    text: 'Mauris vulputate pretium arcu id suscipit.',
                    time: '6:49'
                }
            },
        }
    }
};

export function chatReducer(state = initialState, action) {
    switch (action.type) {
        case 'SEND_MESSAGE':
            let newId = 0;    //id нового сообщения
            let newState = JSON.stringify(state);
            let now = new Date(),
                minutes = String(now.getMinutes());
            minutes = (minutes.length < 2) ? '0' + minutes : minutes;
            now = String(now.getHours()) + ':' + minutes;
            newState = JSON.parse(newState);
            for (let keyMsg in newState.dialogs[action.dialogID].messages) {
                if (!newState.dialogs[action.dialogID].messages.hasOwnProperty(keyMsg)) continue;
                newId = newState.dialogs[action.dialogID].messages[keyMsg].id;
            }
            newId++;
            newState.dialogs[action.dialogID].messages = {
                ...newState.dialogs[action.dialogID].messages,
                [newId]: {
                    id: newId,
                    sent: true,
                    text: action.text,
                    time: now
                }
            };
            return newState;
        default:
            return state;
    }
}

export const reducers = combineReducers({
    chatReducer,
});
